﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.Entity;
using WebAPI_NETCore.Connection;
using WebAPI_NETCore.Controllers;
using WebAPI_NETCore.Models;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace WebAPIConsume_NETCore.Controllers
{
    [Route("API/[controller]/[action]")]
    [ApiController]
    public class ConsumeAPIController : ControllerBase
    {
        #region Property Connection Database
        private readonly ApplicationDBContext DBContext;
        private readonly ILogger<ConsumeAPIController> Logger;

        public ConsumeAPIController(ILogger<ConsumeAPIController> logger, ApplicationDBContext dbcontext)
        {
            Logger = logger;
            DBContext = dbcontext;
        }
        #endregion

        #region Consume API
        [HttpGet, Produces("application/json")]
        public async Task<dynamic> ConsumeGetAllDataMasterMenu()
        {
            return null;
        }


        [HttpGet, Produces("application/json")]
        public async Task<dynamic> ConsumeGetAllDataMasterEmploee()
        {
            return null;
        }

        [HttpGet, Produces("application/json")]
        public IEnumerable<MasterMenu> ConsumeDaftarPemesan()
        {
            return null;
        }
        #endregion


        #region Example Get Primary Table With Relation
        [HttpGet, Produces("application/json")]
        public IEnumerable<MasterMenu> ConsumePrimaryTableWithRelation()
        {
            var GetData = DBContext.MasterMenus.Select(x => new MasterMenu
            {
                Id = x.Id,
                KodeMenu = x.KodeMenu,
                NamaMenu = x.NamaMenu,
                JenisMenu = x.JenisMenu,
                HargaSatuan = x.HargaSatuan,
                DaftarPemesans = x.DaftarPemesans.ToList()
            }).OrderBy(x => x.Id).ToList();

            return GetData;
        }
        #endregion

    }
}
