using Microsoft.EntityFrameworkCore;
using WebAPI_NETCore.Connection;

public partial class Program
{
    static void Main()
    {
        var builder = WebApplication.CreateBuilder();
        #region Connection Database
        var ConfigConnectionStringValue = builder.Configuration["ConnectionStrings:DefaultConnection"];
        builder.Services.AddDbContext<ApplicationDBContext>(options => options.UseSqlServer(ConfigConnectionStringValue));
        builder.Services.AddDbContext<ApplicationDBContext>(options =>
        {
            options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
        });
        #endregion
        
        builder.Services.AddControllers(); //Add services to the container.
        builder.Services.AddEndpointsApiExplorer(); //Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddSwaggerGen();
        var app = builder.Build();
        //Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }
        app.UseHttpsRedirection();
        app.UseAuthorization();
        app.MapControllers();
        app.Run();
    }
}